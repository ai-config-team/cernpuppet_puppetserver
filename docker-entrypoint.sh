#!/bin/sh
set -e

update-alternatives --set java java-1.7.0-openjdk.x86_64

exec /opt/puppetlabs/bin/puppetserver "$@"
