FROM gitlab-registry.cern.ch/linuxsupport/cc7-base

LABEL org.label-schema.maintainer="David Moreno García" \
      org.label-schema.name="Puppet Server" \
      org.label-schema.dockerfile="/Dockerfile"

RUN yum -y upgrade && \
    yum install -y yum-plugin-priorities && \
    yum-config-manager --add-repo "http://linuxsoft.cern.ch/internal/repos/ai7-stable/x86_64/os/" && \
    yum-config-manager --setopt=linuxsoft.cern.ch_internal_repos_ai7-stable_x86_64_os_.priority=1 --save && \
    yum-config-manager --setopt=linuxsoft.cern.ch_internal_repos_ai7-stable_x86_64_os_.gpgcheck=0 --save && \
    yum clean all

RUN mkdir -p /opt/puppetlabs/server/data/puppetserver && \
    groupadd -g 630 -r puppet && \
    useradd -u 630 -g puppet -r -d /opt/puppetlabs/server/data/puppetserver -s /usr/sbin/nologin \
        -c "puppetserver daemon" puppet && \
    yum install -y java-1.7.0-openjdk-headless puppetserver puppetdb-termini && \
    mkdir -p /var/lib/puppet/ssl && chown puppet:puppet /var/lib/puppet/ssl && \
    chown puppet:puppet /etc/pki/tls/certs/CERN-bundle.pem && \
    /opt/puppetlabs/bin/puppetserver gem install deep_merge && \
    /opt/puppetlabs/bin/puppetserver gem install jruby-ldap && \
    /opt/puppetlabs/bin/puppetserver gem install net-ldap:0.12.1

COPY docker-entrypoint.sh /

ENTRYPOINT [ "/docker-entrypoint.sh" ]

CMD [ "foreground" ]
